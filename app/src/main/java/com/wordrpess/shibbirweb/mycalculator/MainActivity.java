package com.wordrpess.shibbirweb.mycalculator;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button numOne, numTwo, numThree, numFour, numFive, numSix, numSeven, numEight, numNine, numZero, numDot, btnAddition, btnSubstraction, btnMultiplicatoin, btnDivision, btnEqual, btnAc;

    double firstValue = Double.NaN;
    double secondValue;
    String secondValueSt = "";

    String operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //numpad assign
        numOne = (Button)findViewById(R.id.num1);
        numTwo = (Button)findViewById(R.id.num2);
        numThree = (Button)findViewById(R.id.num3);
        numFour = (Button)findViewById(R.id.num4);
        numFive = (Button)findViewById(R.id.num5);
        numSix = (Button)findViewById(R.id.num6);
        numSeven = (Button)findViewById(R.id.num7);
        numEight = (Button)findViewById(R.id.num8);
        numNine = (Button)findViewById(R.id.num9);
        numZero = (Button)findViewById(R.id.num0);
        numDot = (Button)findViewById(R.id.numdot);

        //operator assign
        btnAddition = (Button)findViewById(R.id.addition);
        btnSubstraction = (Button)findViewById(R.id.subtraction);
        btnMultiplicatoin = (Button)findViewById(R.id.multiplication);
        btnDivision = (Button)findViewById(R.id.division);
        btnEqual = (Button)findViewById(R.id.equal);

        //additional key
        btnAc = (Button)findViewById(R.id.btnAc);

        //calculator screen
        final EditText calScreen = (EditText)findViewById(R.id.input_screen);
        final TextView result = (TextView)findViewById(R.id.result);

        //button ac function
        btnAc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstValue = Double.NaN;
                secondValueSt = "";
                calScreen.setText("");
            }
        });

        //show number in screen
        numOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String one = numOne.getText().toString();


                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"1";

                }

                calScreen.setText(calScreen.getText() + one);
            }
        });

        numTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String two = numTwo.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"2";

                }

                calScreen.setText(calScreen.getText()+two);
            }
        });

        numThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String three = numThree.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"3";

                }

                calScreen.setText(calScreen.getText()+three);
            }
        });

        numFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String four = numFour.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"4";

                }

                calScreen.setText(calScreen.getText()+four);
            }
        });

        numFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String five = numFive.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"5";

                }

                calScreen.setText(calScreen.getText()+five);
            }
        });

        numSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String six = numSix.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"6";

                }

                calScreen.setText(calScreen.getText()+six);
            }
        });

        numSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String seven = numSeven.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"7";

                }

                calScreen.setText(calScreen.getText()+seven);
            }
        });

        numEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String eight = numEight.getText().toString();


                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"8";

                }

                calScreen.setText(calScreen.getText()+eight);
            }
        });

        numNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nine = numNine.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"9";

                }

                calScreen.setText(calScreen.getText()+nine);
            }
        });

        numZero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String zero = numZero.getText().toString();

                if(!Double.isNaN(firstValue)){

                        secondValueSt = secondValueSt+"0";

                }

                calScreen.setText(calScreen.getText()+zero);
            }
        });

        numDot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String dot = numDot.getText().toString();
                String typedValue = calScreen.getText().toString();
                if(typedValue.indexOf(".") == -1){
                    calScreen.setText(calScreen.getText()+dot);
                }else{
                    calScreen.setText(calScreen.getText());
                }

            }
        });

        /*
        *   Operator assign
        */

        //addition button
        btnAddition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstValue = new Double(calScreen.getText().toString());
                calScreen.setText(calScreen.getText()+"+");
                operation = "Addition";

            }
        });

        //subtraction button
        btnSubstraction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstValue = new Double(calScreen.getText().toString());
                calScreen.setText(calScreen.getText()+"-");
                operation = "Subtraction";

            }
        });

        //multiplication button
        btnMultiplicatoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstValue = new Double(calScreen.getText().toString());
                calScreen.setText(calScreen.getText()+"*");
                operation = "Multiplication";

            }
        });

        //division button
        btnDivision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstValue = new Double(calScreen.getText().toString());
                calScreen.setText(calScreen.getText()+"/");
                operation = "Division";

            }
        });

        //equal button
        btnEqual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(secondValueSt == ""){
                    secondValue = Double.NaN;
                }else{
                    secondValue = Double.parseDouble(secondValueSt);
                }

                if(Double.isNaN(firstValue) || Double.isNaN(secondValue)){
                    calScreen.setText("");
                    firstValue = Double.NaN;
                    secondValueSt = "";
                }else if(firstValue == 0 && secondValue == 0){
                    calScreen.setText("Undefined");
                }else{
                    if(operation == "Addition"){
                        double total = firstValue+secondValue;
                        String totalResult = Double.toString(total);
                        calScreen.setText(totalResult);
                    }else if(operation == "Subtraction"){
                        double total = firstValue-secondValue;
                        String totalResult = Double.toString(total);
                        calScreen.setText(totalResult);
                    }else if(operation == "Multiplication"){
                        double total = firstValue*secondValue;
                        String totalResult = Double.toString(total);
                        calScreen.setText(totalResult);
                    }else if(operation == "Division"){
                        double total = firstValue/secondValue;
                        String totalResult = Double.toString(total);
                        calScreen.setText(totalResult);
                    }
                }
                secondValueSt = "";
            }
        });

    }
}
